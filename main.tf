data "docker_registry_image" "Bazarr" {
	name = "linuxserver/bazarr:latest"
}

resource "docker_image" "Bazarr" {
	name = data.docker_registry_image.Bazarr.name
	pull_triggers = [data.docker_registry_image.Bazarr.sha256_digest]
}

module "Bazarr" {
  source = "gitlab.com/RedSerenity/docker/local"

	name = var.name
	image = docker_image.Bazarr.latest

	networks = [{ name: var.docker_network, aliases: ["bazarr.${var.internal_domain_base}"] }]
  ports = [{ internal: 6767, external: 9603, protocol: "tcp" }]
	volumes = [
		{
			host_path = "${var.docker_data}/Bazarr"
			container_path = "/config"
			read_only = false
		},
		{
			host_path = "${var.movies}"
			container_path = "/movies"
			read_only = false
		},
		{
			host_path = "${var.tv}"
			container_path = "/tv"
			read_only = false
		}
	]

	environment = {
		"PUID": "${var.uid}",
		"PGID": "${var.gid}",
		"TZ": "${var.tz}"
	}

	stack = var.stack
}